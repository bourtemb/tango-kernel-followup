# Tango Kernel Follow-up Meeting - 2020/05/14

To be held on 2020/05/14 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-04-23/Minutes.md#summary-of-remaining-actions)
 2. cppTango News
 3. JTango News
 4. PyTango News
 5. Tango Source Distribution News
 6. Conan packages
 7. Conda packages
 8. Tango Meeting 2020
 9. High priority issues
 10. AOB
     - HDB++