# Tango Kernel Follow-up Meeting - 2020/11/26

To be held on 2020/11/26 at 15:00 CET on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-11-12/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2020-11-26)
 3. Tango Kernel Teleconf Meetings reorganization
 4. High priority issues
 5. Tango Kernel Webinars
 6. Roadmap (V10, LTS version(s))
 7. Travis-ci.org shutdown ([cppTango#812](https://github.com/tango-controls/cppTango/issues/812))
 8. Conda packages
 9. AOB
