# Tango Kernel Follow-up Meeting - 2021/02/25

To be held on 2021/02/25 at 15:00 CET on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2021/2021-02-11/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-02-25)
 3. Gitlab migration ([TangoTickets#47](https://github.com/tango-controls/TangoTickets/issues/47))
 4. High priority issues
 5. AOB
