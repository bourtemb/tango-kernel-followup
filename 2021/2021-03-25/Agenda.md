# Tango Kernel Follow-up Meeting - 2021/03/25

To be held on 2021/03/25 at 15:00 CET on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/tango-kernel-followup/-/blob/main/2021/2021-03-11/Minutes.md#summary-of-remaining-actions)
 2. Tango Collaboration Contract
 3. [Tango RFCs](https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-03-25)
 4. High priority issues
 5. Gitlab migration ([TangoTickets#47](https://github.com/tango-controls/TangoTickets/issues/47))
 6. Tango Kernel Webinars
 7. AOB
 
